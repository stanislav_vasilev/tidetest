SOLUTION TO TECHNICAL TEST FROM Tide.co

Solution is a Maven project. As a persistence layer I used H2 with Ebean as an ORM to connect and map object.

For this test I attempted to structure the code to mimic a standard MVC structure, missing the Views as we don't need them.
In its current state this API would only accept GET, PUT, POST, DELETE requests.

Took me just over 4h to complete the test including bootstrapping the project, getting familiar with the HttpServer and the other tools used in the project.

NOTE: API would only accept url encoded body for POST/PUT requests.

Potential improvements:
1. Multithreaded handler for the requests
2. Tests for all controllers. Currently only LikeStory and SetStory have been tested. Tests for the rest would be very similar. 


To test:
> mvn3 test

To package:
> mvn3 package

To run:
> mvn3 java -jar target/StoryTest-0.0.1-SNAPSHOT.jar 
This would run the server on port 9002 locally. 