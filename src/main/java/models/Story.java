package models;


import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OptimisticLockException;
import javax.persistence.Table;
import javax.persistence.Version;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.Model;


@Entity
@Table(name="stories")
public class Story extends Model {

	@Id
	private Long id;
	
	private Integer popularity = 0;

	@Version
    private Timestamp ts;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getPopularity() {
		return popularity;
	}

	public void setPopularity(Integer popularity) {
		this.popularity = popularity;
	}
	
	public Timestamp getTs() {
		return ts;
	}

	public void setTs(Timestamp ts) {
		this.ts = ts;
	}

	public static Story getById(Integer id) {
		
		Story theStory = Ebean.getServer(null).find(Story.class).where().eq("id", id.longValue()).findUnique();
		return theStory;
	}
	public void incrementPopularity(Integer inc, Integer tries) throws OptimisticLockException {
		this.popularity += inc;
		while(tries-- >= 0) {
			try {
				Ebean.getServer(null).save( this );
				return;
			} catch(OptimisticLockException e) {
				Story story = Story.getById(this.getId().intValue());
				this.setPopularity(story.getPopularity()+1);
				this.setTs(story.getTs());
				continue;
			}
		}
		throw new OptimisticLockException();
	}
	public void incrementPopularity(Integer tries) throws OptimisticLockException  {
		this.incrementPopularity(1, tries);
	}
	
}
