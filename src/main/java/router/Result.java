package router;

import java.util.Map;

import com.google.gson.Gson;

public class Result {

	private Integer code;
	private String body;
	
	public Result(Integer c, String body) {
		this.code = c;
		this.body = body;
	}
	public Result(Integer c, Map<String, ? extends Object> returnJson) {
		Gson gson = new Gson();
		this.code = c;
		this.body = gson.toJson(returnJson);;
	}
	
	public Integer getCode() {
		return code;
	}
	
	public String getBody() {
		return body;
	}
	
}
