package router;


import controllers.AbstractAction;
import jauter.Router;

public class ProjectRouter extends Router<AcceptedMethos, Class<? extends AbstractAction>, ProjectRouter> {

	@Override protected ProjectRouter getThis() { return this; }

	@Override protected AcceptedMethos CONNECT() { return AcceptedMethos.CONNECT; }
	@Override protected AcceptedMethos DELETE()  { return AcceptedMethos.DELETE ; }
	@Override protected AcceptedMethos GET()     { return AcceptedMethos.GET    ; }
	@Override protected AcceptedMethos HEAD()    { return AcceptedMethos.HEAD   ; }
	@Override protected AcceptedMethos OPTIONS() { return AcceptedMethos.OPTIONS; }
	@Override protected AcceptedMethos PATCH()   { return AcceptedMethos.PATCH  ; }
	@Override protected AcceptedMethos POST()    { return AcceptedMethos.POST   ; }
	@Override protected AcceptedMethos PUT()     { return AcceptedMethos.PUT    ; }
	@Override protected AcceptedMethos TRACE()   { return AcceptedMethos.TRACE  ; }

	//TODO: implement the rest of the methods
	public AcceptedMethos getMethod(String input) {
		AcceptedMethos method = AcceptedMethos.GET;
		if( input.toUpperCase().equals("POST") )
			method = AcceptedMethos.POST;
		else if( input.toUpperCase().equals("PUT") )
			method = AcceptedMethos.PUT;
		else if( input.toUpperCase().equals("GET") )
			method = AcceptedMethos.GET;
		else if( input.toUpperCase().equals("DELETE") )
			method = AcceptedMethos.DELETE;
		return method;
	}

}
