package router;

public enum AcceptedMethos {
  CONNECT, DELETE, GET, HEAD, OPTIONS, PATCH, POST, PUT, TRACE
}