package controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;

import router.Result;

public class NotFoundAction extends AbstractAction {

	public Result execute(Map<String, Object> params) {
		
		Map<String, String> resp = new HashMap<String, String>();
		resp.put("msg", "Not Found");
		return new Result(404, resp);
	}

	@Override
	protected Boolean validateRequest(Map<String, Object> params, Map<String, List<String>> errors) {
		return true;
	}


}
