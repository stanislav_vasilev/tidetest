package controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import router.Result;

public abstract class AbstractAction {

	protected Integer optimisticLockRetries = 3;
	
	abstract public Result execute(Map<String, Object> params);
	protected void addValidationError(Map<String, List<String>> errors, final String key, final String value) {
		if( !errors.containsKey(key) ) {
			errors.put(key, new ArrayList<String>());
		}
		errors.get(key).add(value);
	}
	
	protected Boolean validateRequest(Map<String, Object> params, Map<String, List<String>> errors) {
		if( !params.containsKey("id") ) 
			this.addValidationError(errors, "id", "Field is required");
		else if( !params.get("id").toString().matches("\\d+$") ) // this can be done with more code but more efficiently checking each character
			this.addValidationError(errors, "id", "Field must be positive integer");
		
		return errors.isEmpty();
	}
	
}
