package controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.OptimisticLockException;

import com.google.gson.Gson;

import models.Story;
import router.Result;

public class LikeStory extends AbstractAction {

	public Result execute(Map<String, Object> params) {
		Map<String, List<String>> errors = new HashMap<String, List<String>>();

		if( !validateRequest(params, errors) ) 
			return new Result(400, errors);
		
		Integer id = Integer.valueOf( params.get("id").toString() );
		Story theStory = Story.getById( id );
		Map<String, Object> returnVal = new HashMap<String, Object>();
		Integer returnCode = 400;
		if( theStory == null ) {
			returnVal.put("msg", "Story not found");
			returnCode = 404;
		} else {
			try {
				theStory.incrementPopularity(1, this.optimisticLockRetries);
				returnCode = 200;
				returnVal.put("popularity", theStory.getPopularity());
			} catch( OptimisticLockException e ) {
				returnCode = 500;
				returnVal.put("msg", "Optimistic lock issue");
			}
		}
		return new Result(returnCode,  returnVal);
	}

	

}
