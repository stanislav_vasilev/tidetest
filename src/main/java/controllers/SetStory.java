package controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.OptimisticLockException;

import com.avaje.ebean.Ebean;

import models.Story;
import router.Result;

public class SetStory extends AbstractAction {

	public Result execute(Map<String, Object> params) {
		Map<String, List<String>> errors = new HashMap<String, List<String>>();

		if( !validateRequest(params, errors) ) 
			return new Result(400, errors);
		
		Integer id = Integer.valueOf( params.get("id").toString() );
		Story theStory = Story.getById( id );
		Map<String, Object> returnVal = new HashMap<String, Object>();
		Integer returnCode = 400;
		if( theStory == null ) {
			returnVal.put("msg", "Story not found");
			returnCode = 404;
		} else {	
			
			while(this.optimisticLockRetries-- >= 0) {
				try {
					theStory.setPopularity( Integer.valueOf( params.get("popularity").toString() ) );
					Ebean.getServer(null).update(theStory);
					returnCode = 200;
					returnVal.put("popularity", theStory.getPopularity());
					returnVal.remove("msg");
					break;
				} catch(OptimisticLockException e) {
					theStory = Story.getById( id );
					returnCode = 500;
					returnVal.put("msg", "Optimistic Lock issue");
				}
			}
		}
		
		return new Result(returnCode,  returnVal);
	}

	@Override
	protected Boolean validateRequest(Map<String, Object> params, Map<String, List<String>> errors) {
		super.validateRequest(params, errors);
		
		if( !params.containsKey("popularity") ) 
			this.addValidationError(errors, "popularity", "Field is required");
		else if( !params.get("popularity").toString().matches("^-?\\d+$") ) // this can be done with more code but more efficiently checking each character
			this.addValidationError(errors, "popularity", "Field must be integer");
		
		return errors.isEmpty();
	}

}
