package controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;

import models.Story;
import router.Result;

public class ViewStory extends AbstractAction {

	public Result execute(Map<String, Object> params) {
		Map<String, List<String>> errors = new HashMap<String, List<String>>();

		if( !validateRequest(params, errors) ) 
			return new Result(400, errors);
		
		System.out.println( errors.toString() );
		
		Integer id = Integer.valueOf( params.get("id").toString() );
		Story theStory = Story.getById( id );
		Map<String, Object> returnVal = new HashMap<String, Object>();
		Integer returnCode = 400;
		if( theStory == null ) {
			returnVal.put("msg", "Story not found");
			returnCode = 404;
		} else {
			returnVal.put("popularity", theStory.getPopularity() );
			returnCode = 200;
		}
		return new Result(returnCode, returnVal );
	}


}
