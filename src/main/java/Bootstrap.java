import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.util.concurrent.Executors;

import com.avaje.ebean.EbeanServerFactory;
import com.avaje.ebean.config.ServerConfig;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

import controllers.AbstractAction;
import controllers.DislikeStory;
import controllers.LikeStory;
import controllers.NotFoundAction;
import controllers.SetStory;
import controllers.ViewStory;
import helpers.RequestParameters;
import jauter.Routed;
import models.Story;
import router.ProjectRouter;
import router.Result;


public class Bootstrap {
	
	public static void main(String[] args) throws Exception{
		
		Bootstrap.setupDbConnection();
		
		DataLoad.initData();
		
		final HttpServer httpServer;
		httpServer = HttpServer.create(new InetSocketAddress(9002), 0);
		httpServer.setExecutor(Executors.newCachedThreadPool());
		httpServer.createContext("/", new HttpHandler() {
			public void handle(HttpExchange httpExchange) throws IOException {
				ProjectRouter router = new ProjectRouter()
						.GET	("/story/:id",			ViewStory.class)
						.POST	("/story/:id",			SetStory.class)
						.PUT	("/story/:id/like",		LikeStory.class)
						.PUT	("/story/:id/dislike",	DislikeStory.class)
						.notFound (NotFoundAction.class);
				
				
				Routed<Class<? extends AbstractAction>> routed1 = router.route(router.getMethod(httpExchange.getRequestMethod()), httpExchange.getRequestURI().getPath() );
				RequestParameters requestParameters = new RequestParameters(router.getMethod(httpExchange.getRequestMethod()), routed1.params(), httpExchange.getRequestBody() );
				
				AbstractAction controller;
				Result result;
				try {
					controller = routed1.target().newInstance();
					result = controller.execute( requestParameters.getRequestParameters() );
				} catch (Exception e) {
					result = new Result(500, "{}");
					e.printStackTrace();
				}
				
				final byte[] out = result.getBody().getBytes("UTF-8");
				httpExchange.getResponseHeaders().add("Content-Type", "application/json");
				httpExchange.sendResponseHeaders(result.getCode(), out.length);
//				Writer out = new OutputStreamWriter(httpExchange.getResponseBody(), "UTF-8"));
//				out.write(something);
				OutputStream os = httpExchange.getResponseBody();
				os.write(out);
				os.close();
			}
		});
		httpServer.start();
	
		
	}
	
	private static void setupDbConnection() {
		ServerConfig config = new ServerConfig();
		config.setName("db");
		config.setDefaultServer(true);
		config.loadFromProperties();
		config.addClass(Story.class);
		EbeanServerFactory.create(config);
	}
	
}
