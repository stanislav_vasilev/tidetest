package helpers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import router.AcceptedMethos;

public class RequestParameters {

	private AcceptedMethos method;
	private Map<String, Object> params;
	private InputStream requestBody;
	
	public RequestParameters(AcceptedMethos method, Map<String, String> params, InputStream requestBody) {
		super();
		this.method = method;
		this.params = new HashMap<String, Object>(params);
		this.requestBody = requestBody;
	}
	
	public Map<String, Object> getRequestParameters() throws IOException {
		if( method.equals( AcceptedMethos.POST ) || method.equals( AcceptedMethos.PUT ) ) {
			InputStreamReader isr =  new InputStreamReader(requestBody,"utf-8");
			BufferedReader br = new BufferedReader(isr);
	
			int b;
			StringBuilder buf = new StringBuilder(512);
			while ((b = br.read()) != -1) {
			    buf.append((char) b);
			}
			br.close();
			isr.close();
			System.out.println( buf.toString() );
			parseQuery(buf.toString(), this.params);
		}
		
		return params;
	}
	
	@SuppressWarnings("unchecked")
    private void parseQuery(String query, Map<String, Object> parameters) throws UnsupportedEncodingException {
        if (query != null) {
            String pairs[] = query.split("[&]");

            for (String pair : pairs) {
                String param[] = pair.split("[=]");
                String key = null;
                String value = null;
                if (param.length > 0) 
                    key = URLDecoder.decode(param[0], "UTF-8");

                if (param.length > 1) 
                    value = URLDecoder.decode(param[1], "UTF-8");

                if (parameters.containsKey(key)) {
                    Object obj = parameters.get(key);
                    if(obj instanceof List<?>) {
                        List<String> values = (List<String>)obj;
                        values.add(value);
                    } else if(obj instanceof String) {
                        parameters.put(key, value);
                    }
                } else {
                    parameters.put(key, value);
                }
            }
        }
   }
	
}
