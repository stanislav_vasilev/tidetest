import org.h2.util.IOUtils;
import org.junit.Test;
import org.mockito.Mockito;

import com.avaje.ebean.Ebean;
import com.google.gson.Gson;
import com.sun.net.httpserver.HttpExchange;

import controllers.AbstractAction;
import controllers.DislikeStory;
import controllers.LikeStory;
import controllers.NotFoundAction;
import controllers.SetStory;
import controllers.ViewStory;
import helpers.RequestParameters;
import jauter.Routed;
import models.Story;
import router.AcceptedMethos;
import router.ProjectRouter;
import router.Result;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;


public class TestRoutes {

	@Test
	public void testAcceptedMethods() {
		
		ProjectRouter router = new ProjectRouter()
				.GET	("/story/:id",			ViewStory.class)
				.POST	("/story/:id",			SetStory.class)
				.PUT	("/story/:id/like",		LikeStory.class)
				.PUT	("/story/:id/dislike",	DislikeStory.class)
				.notFound (NotFoundAction.class);
		
		HttpExchange httpExchange = mock(HttpExchange.class);
		
		when(httpExchange.getRequestMethod()).thenReturn("GET");		
		assertEquals(AcceptedMethos.GET, router.getMethod(httpExchange.getRequestMethod()));
		
		when(httpExchange.getRequestMethod()).thenReturn("POST");		
		assertEquals(AcceptedMethos.POST, router.getMethod(httpExchange.getRequestMethod()));
		
		when(httpExchange.getRequestMethod()).thenReturn("PUT");		
		assertEquals(AcceptedMethos.PUT, router.getMethod(httpExchange.getRequestMethod()));
		
		when(httpExchange.getRequestMethod()).thenReturn("DELETE");		
		assertEquals(AcceptedMethos.DELETE, router.getMethod(httpExchange.getRequestMethod()));
		
	}
	
	@Test
	public void testRoutes() throws URISyntaxException, InstantiationException, IllegalAccessException {
		
		ProjectRouter router = new ProjectRouter()
				.GET	("/story/:id",			ViewStory.class)
				.POST	("/story/:id",			SetStory.class)
				.PUT	("/story/:id/like",		LikeStory.class)
				.PUT	("/story/:id/dislike",	DislikeStory.class)
				.notFound (NotFoundAction.class);
		Routed<Class<? extends AbstractAction>> routed1;
		
		HttpExchange httpExchange = mock(HttpExchange.class);
		when(httpExchange.getRequestMethod()).thenReturn("GET");
		when(httpExchange.getRequestURI()).thenReturn( new URI("/bad-url") );
		routed1 = router.route(router.getMethod(httpExchange.getRequestMethod()), httpExchange.getRequestURI().getPath() );
		assertEquals(true, routed1.target().newInstance() instanceof NotFoundAction );
		
		when(httpExchange.getRequestURI()).thenReturn( new URI("/story/10") );
		routed1 = router.route(router.getMethod(httpExchange.getRequestMethod()), httpExchange.getRequestURI().getPath() );
		assertEquals(true, routed1.target().newInstance() instanceof ViewStory );
		
		when(httpExchange.getRequestMethod()).thenReturn("POST");
		when(httpExchange.getRequestURI()).thenReturn( new URI("/story/10") );
		routed1 = router.route(router.getMethod(httpExchange.getRequestMethod()), httpExchange.getRequestURI().getPath() );
		assertEquals(true, routed1.target().newInstance() instanceof SetStory );
		
		when(httpExchange.getRequestURI()).thenReturn( new URI("/story/10/like") );
		routed1 = router.route(router.getMethod(httpExchange.getRequestMethod()), httpExchange.getRequestURI().getPath() );
		assertEquals(true, routed1.target().newInstance() instanceof NotFoundAction );
		
		when(httpExchange.getRequestMethod()).thenReturn("PUT");
		routed1 = router.route(router.getMethod(httpExchange.getRequestMethod()), httpExchange.getRequestURI().getPath() );
		assertEquals(true, routed1.target().newInstance() instanceof LikeStory );
		
		when(httpExchange.getRequestURI()).thenReturn( new URI("/story/10/dislike") );
		routed1 = router.route(router.getMethod(httpExchange.getRequestMethod()), httpExchange.getRequestURI().getPath() );
		assertEquals(true, routed1.target().newInstance() instanceof DislikeStory );
		
	}
	
	@Test
	public void testRequestParameters() throws IOException {
		
		InputStream stubInputStream = IOUtils.getInputStreamFromString("popularity=20");
		InputStream badStubInputStream = IOUtils.getInputStreamFromString("popularity:a20");
		
		Map<String, String> goodId = new HashMap<String, String>();
		goodId.put("id", "10");
		
		RequestParameters params = new RequestParameters(AcceptedMethos.POST, goodId, stubInputStream);
		
		Map<String, Object> requestPrams = params.getRequestParameters();
		
		assertEquals(true, requestPrams.containsKey("id"));
		assertEquals(true, requestPrams.containsKey("popularity"));
		assertEquals(true, requestPrams.get("popularity").equals("20"));
		assertEquals(true, requestPrams.get("id").equals("10"));
		
		params = new RequestParameters(AcceptedMethos.POST, goodId, badStubInputStream);
		requestPrams = params.getRequestParameters();
		
		assertEquals(true, requestPrams.containsKey("id"));
		assertEquals(false, requestPrams.containsKey("popularity"));
		assertEquals(true, requestPrams.get("id").equals("10"));
		
	}
	
	//TODO: test for DislikeStory
	//TODO: test for ViewStory
	
	@Test
	public void testLikeStoryController() throws IOException {
		RequestParameters params = mock(RequestParameters.class);
		Map<String, Object> goodId = new HashMap<String, Object>();
		goodId.put("id", 999);
		
		
		Map<String, Object> badId1 = new HashMap<String, Object>();
		badId1.put("id", "aaa");
		
		Map<String, Object> badId2 = new HashMap<String, Object>();
		badId2.put("id", -10);
		
		LikeStory controller = new LikeStory();
		
		when(params.getRequestParameters()).thenReturn(goodId);
		assertEquals(404,  controller.execute(params.getRequestParameters()).getCode().intValue() );
		
		Story s = new Story();
		s.setId(999l);
		Ebean.getServer(null).save(s);
		
		Gson gson = new Gson();
		
		when(params.getRequestParameters()).thenReturn(goodId);
		Result result = controller.execute(params.getRequestParameters());
		assertEquals(200,  result.getCode().intValue() );
		assertEquals("1.0",  gson.fromJson(result.getBody(), Map.class).get("popularity").toString() );
		
		s = Ebean.find(Story.class).where().eq("id", s.getId()).findUnique();
		assertEquals(1, s.getPopularity().intValue());
		
		when(params.getRequestParameters()).thenReturn(badId1);
		assertEquals(400,  controller.execute(params.getRequestParameters()).getCode().intValue() );
		
		when(params.getRequestParameters()).thenReturn(badId2);
		assertEquals(400,  controller.execute(params.getRequestParameters()).getCode().intValue() );
		
		Ebean.getServer(null).delete(s);
		
	}
	
	@Test
	public void testSetStoryController() throws IOException {
		RequestParameters params = mock(RequestParameters.class);
		Map<String, Object> goodId = new HashMap<String, Object>();
		goodId.put("id", 10);
		goodId.put("popularity", 20);
		
		Map<String, Object> goodIdBadPopularity = new HashMap<String, Object>();
		goodIdBadPopularity.put("id", 10);
		goodIdBadPopularity.put("popularity", "a20");
		
		Map<String, Object> badId1 = new HashMap<String, Object>();
		badId1.put("id", "aaa");
		
		Map<String, Object> badId2 = new HashMap<String, Object>();
		badId2.put("id", -10);
		
		Gson gson = new Gson();
		
		SetStory controller = new SetStory();
		
		when(params.getRequestParameters()).thenReturn(goodId);
		assertEquals(404,  controller.execute(params.getRequestParameters()).getCode().intValue() );
		
		Story s = new Story();
		s.setId(10l);
		Ebean.getServer(null).save(s);
		
		when(params.getRequestParameters()).thenReturn(goodId);
		Result result = controller.execute(params.getRequestParameters());
		assertEquals(200,  result.getCode().intValue() );
		assertEquals("20.0",  gson.fromJson(result.getBody(), Map.class).get("popularity").toString() );
		//TODO: test optimistic lock case
		
		when(params.getRequestParameters()).thenReturn(goodIdBadPopularity);
		assertEquals(400,  controller.execute(params.getRequestParameters()).getCode().intValue() );
		
		when(params.getRequestParameters()).thenReturn(badId1);
		assertEquals(400,  controller.execute(params.getRequestParameters()).getCode().intValue() );
		
		when(params.getRequestParameters()).thenReturn(badId2);
		assertEquals(400,  controller.execute(params.getRequestParameters()).getCode().intValue() );
		
	}
	
}
