import org.junit.Test;

import com.google.gson.Gson;

import router.Result;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.util.HashMap;
import java.util.Map;

public class TestResult {

	@Test
	public void testTextResult() {
		
		Result res = new Result(200, "Test");
		
		assertEquals(true, res.getBody().equals("Test"));
		
		Map<String, Object> resp = new HashMap<String, Object>();
		resp.put("msg", "test msg");
		resp.put("val", 2);
		
		res = new Result(200, resp);
		Gson gson = new Gson();
		assertEquals(true, res.getBody().equals( gson.toJson( resp ) ));
		
	}
	
}
