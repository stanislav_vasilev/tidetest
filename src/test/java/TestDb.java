import org.junit.Test;
import org.mockito.Mockito;

import com.avaje.ebean.Ebean;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import javax.persistence.OptimisticLockException;

import models.Story;

public class TestDb {

	@Test
	public void testConnection() {
		
		int storiesCount = Ebean.getServer(null).find(Story.class).findCount();
		assertEquals(storiesCount, 0);
		
		Story s = new Story();
		s.setPopularity(0);
		Ebean.getServer(null).save(s);
		storiesCount = Ebean.getServer(null).find(Story.class).findCount();
		assertEquals(storiesCount, 1);
		Ebean.getServer(null).delete(s);
		
		storiesCount = Ebean.getServer(null).find(Story.class).findCount();
		assertEquals(storiesCount, 0);
		
	}
	
	@Test
	public void testStory() {
		Story test = Mockito.mock(Story.class);
		assertEquals(test.getPopularity().intValue(), 0);
	}
	
	@Test(expected=OptimisticLockException.class)
	public void testStoryOptimisticLock() {
		Story testStory = new Story();
		Ebean.getServer(null).save(testStory);
		Story testStoryFromDb = Ebean.getServer(null).find(Story.class).where().eq("id", testStory.getId()).findUnique();
		testStory.setPopularity(10);
		Ebean.getServer(null).save(testStory);
		
		testStoryFromDb.setPopularity(10);
		Ebean.getServer(null).save(testStoryFromDb);
	}
	
}
